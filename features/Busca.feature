#language: pt
Funcionalidade: Busca

  Contexto: 
    Dado que eu esteja na home do site getnet

  @busca
  Cenário: Busca - Exibição de Ajuda com Portabilidade da Maquininha
    Quando busco por "superget"
    E no resultado da busca, clico no link "Como faço a portabilidade da minha maquininha"
    Então um modal é visualizado contendo a mensagem "Como faço a portabilidade da minha maquininha?"

  @busca
  Cenário: Busca - Exibição de Ajuda com Cancelamento
    Quando busco por "superget"
    E no resultado da busca, clico no link "Como cancelar a minha Conta SuperGet?"
    Então um modal é visualizado contendo a mensagem "Como cancelar a minha Conta SuperGet?"
