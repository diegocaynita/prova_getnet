package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.BasePage;

public class Home extends BasePage {

	public Home(WebDriver driver) {
		super(driver);

	}

	WebDriverWait wait = new WebDriverWait(driver, 25);

	// Locators
	@FindBy(id = "search-trigger")
	public WebElement trgSearch;

	@FindBy(id = "global-search-input")
	public WebElement inpSearch;

	@FindBy(className = "c-search-box__button")
	public WebElement btnSearch;

	public void search(String q) {
		wait.until(ExpectedConditions.visibilityOf(trgSearch)).click();
		// assertTrue("Campo de busca não foi exibido.", inpSearch.isDisplayed());
		wait.until(ExpectedConditions.visibilityOf(inpSearch)).sendKeys(q);
		wait.until(ExpectedConditions.elementToBeClickable(btnSearch)).click();
	}

}
