package pages;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.BasePage;

public class SearchResults extends BasePage {

	public SearchResults(WebDriver driver) {
		super(driver);

	}

	WebDriverWait wait = new WebDriverWait(driver, 25);

	// Locators

	@FindBy(className = "c-search-page__result-title")
	public List<WebElement> results;

	@FindBy(className = "page-numbers")
	public List<WebElement> pages;

	@FindBy(xpath = "//*[@class='o-modal is-modal-open']//div[@class='o-modal__title']")
	public WebElement modalTitle;

	@FindBy(className = "is-modal-open")
	public WebElement openedModal;

	public void clickOnSpecificResult(String text) {
		String textToSearch = text.toUpperCase();
		boolean notFound = true;
		while (notFound) {
			wait.until(ExpectedConditions.visibilityOfAllElements(results));
			for (WebElement r : results) {
				if (r.getText().toUpperCase().contains(textToSearch)) {
					r.click();
					notFound = false;
					break;
				}
			}
			int i = 0;
			for (WebElement p : pages) {
				if (p.getAttribute("class").contains("current")) {
					assertTrue("O resultado desejado não foi encontrado.",
							!pages.get(pages.size() - 1).getAttribute("class").contains("current"));
					clicarElemento(pages.get(i + 1));
					i = 0;
					break;
				} else {
					i++;
				}

			}

		}
	}

	public void validateTitleFromModal(String title) {
		wait.until(ExpectedConditions.visibilityOf(openedModal));
		assertTrue(modalTitle.getText().toUpperCase().contains(title.toUpperCase()));
	}
}
