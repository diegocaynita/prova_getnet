package stepDefinitions;

import core.Constants;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import pages.Home;
import pages.SearchResults;

public class SearchSteps {
	Home home = new Home(Constants.WebDriver);
	SearchResults results = new SearchResults(Constants.WebDriver);

	@Quando("busco por {string}")
	public void busco_por(String string) {
		home.search(string);
	}

	@Quando("no resultado da busca, clico no link {string}")
	public void no_resultado_da_busca_clico_no_link(String text) {
		results.clickOnSpecificResult(text);
	}

	@Então("um modal é visualizado contendo a mensagem {string}")
	public void um_modal_é_visualizado_contendo_a_mensagem(String title) {
		results.validateTitleFromModal(title);
	}
}
