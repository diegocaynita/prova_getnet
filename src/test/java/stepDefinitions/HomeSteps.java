package stepDefinitions;

import java.io.IOException;

import core.Constants;
import core.WebDriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.pt.Dado;

public class HomeSteps {
	static WebDriverFactory wdf;
	String urlHome = "https://site.getnet.com.br/";

	@Dado("que eu esteja na home do site getnet")
	public void que_esteja_na_home_do_site_getnet() {
		wdf = new WebDriverFactory();
		try {
			wdf.openUrl(urlHome);
		} catch (IOException e) {
			throw new Error("Não foi possível abrir o endereço informado. Verifique sua conexão e tente novamente.");
		}

	}

	@After
	static public void tearDown() {
		Constants.WebDriver.quit();
	}

}
