package core;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BasePage {

	protected WebDriver driver;

	public BasePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	protected void clicarElemento(WebElement e) {
		JavascriptExecutor exec = (JavascriptExecutor) driver;
		exec.executeScript("arguments[0].click();", e);
	}
}