package core;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverFactory {

	protected static WebDriver webDriver = null;

	public void setUpBrowser() throws MalformedURLException {

		String os = System.getProperty("os.name");

		if (os.contains("Mac")) {
			webDriver = getWebDriverChrome(Constants.CONST_MACOS_CHROMEDRIVER_PATH);
		} else {
			webDriver = getWebDriverChrome(Constants.CONST_WINDOWS_CHROMEDRIVER_PATH);
		}
		Constants.WebDriver = webDriver;
	}

	public void openUrl(String url) throws IOException {
		setUpBrowser();
		webDriver = Constants.WebDriver;
		webDriver.get(url);
		webDriver.manage().window().maximize();

	}

	public static WebDriver getBrowser(String browserType, WebDriver driver) {
		System.setProperty("webdriver.chrome.driver", Constants.ChromeDriver);
		driver = new ChromeDriver();
		return driver;
	}

	public static WebDriver getWebDriverChrome(String constants) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		System.setProperty(Constants.CONST_CHROMEDRIVER_PROPERTY, constants);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--unlimited-storage");

		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);

		return new ChromeDriver(capabilities);
	}

	public static Boolean validarCaminhoDiretorio(String path) {

		Boolean result = false;
		File diretorio = new File(path);
		if (diretorio.exists()) {
			result = true;
		}

		return result;

	}

}
