package core;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.openqa.selenium.WebDriver;

public class Constants {

	// Constantes de drivers
	public static WebDriver WebDriver;
	public static String ChromeDriver = System.getProperty("user.dir") + "//driver//chromedriver";

	// Variáveis Browser
	public static String Name_Chrome = "CHROME";

	// Constants WebDriver

	public static final String CONST_CHROMEDRIVER_PROPERTY = "webdriver.chrome.driver";
	public static final String CONST_MACOS_CHROMEDRIVER_PATH = System.getProperty("user.dir")
			+ "/src/test/drivers/chromedriver";
	public static final String CONST_WINDOWS_CHROMEDRIVER_PATH = System.getProperty("user.dir")
			+ "/src/test/drivers/chromedriver.exe";

	
}
