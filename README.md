<b>Execução</b>

- O projeto é executado a partir da classe Runner, onde é passada a tag que indica quais cenários irão ser executados.
- O projeto já inclui o Chromedriver


<b>Notas</b>

Ao verificar o fluxo descrito no teste, foi verificado que o texto informado não exibe a opção pedida. Logo, o teste que valida este item em específico provavelmente irá falhar.
Pensando nisso, criei um segundo cenário com uma pesquisa que retorna alguma opção e faz o mesmo fluxo que o solicitado. Dessa forma é possível perceber que o código em si está funcionando e o problema é decorrente da aplicaçao testada.